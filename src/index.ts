import './styles/style.css';
// board
class Board{
    private cells:any;
    // constructor(){
    //     this.cells;
    // }
    create():any{
        this.cells = document.createElement('div');
        this.cells.id = 'game';
        document.body.appendChild(this.cells);
        // console.log(this.cells);
        for(let i=0;i<9;i++){
            console.log(document.getElementById('game'));
            document.getElementById('game').innerHTML+='<div class="cell"></div>';
            
        }
        return document.getElementsByClassName('cell');
    }
    remove():void{
        this.cells.parentElement.removeChild(this.cells);
    }
}
// player
class Player {
    name:String;
    constructor(name){
        this.name=name;
    }
}
// game
class Game{
    step:any;
    X:Player;
    O:Player;
    board:Board;
    block:any;
    COMBO:any[];
    constructor(){
        this.step=0;//ходы
        this.X=new Player("X");
        this.O=new Player("O");
        this.board=new Board();
        this.block=this.board.create();
        this.COMBO = [
            [this.block[0],this.block[1],this.block[2]],
            [this.block[3],this.block[4],this.block[5]],
            [this.block[6],this.block[7],this.block[8]],
            [this.block[0],this.block[3],this.block[6]],
            [this.block[1],this.block[4],this.block[7]],
            [this.block[2],this.block[5],this.block[8]],
            [this.block[0],this.block[4],this.block[8]],
            [this.block[2],this.block[4],this.block[6]]
        ];//матрица победных комбинаций
        document.getElementById('game').onclick=(event)=>game.next();
    }
    checkWinner():void{
        for (var i=0; i<this.COMBO.length; i++) {
            if (this.COMBO[i][0].innerHTML==this.COMBO[i][1].innerHTML
                &&this.COMBO[i][1].innerHTML==this.COMBO[i][2].innerHTML
                &&this.COMBO[i][0].innerHTML==this.O.name){
                alert('Победа ноликов!');
                this.board.remove();
                this.board=new Board();
                this.step=0;
                var game=new Game();
                document.getElementById('game').onclick=(event)=>game.next();
            }
            else if (this.COMBO[i][0].innerHTML==this.COMBO[i][1].innerHTML
                &&this.COMBO[i][1].innerHTML==this.COMBO[i][2].innerHTML
                &&this.COMBO[i][0].innerHTML==this.X.name){
                alert('Победа крестиков!');
                this.board.remove();
                this.board=new Board();
                this.step=0;
                var game=new Game();
                document.getElementById('game').onclick=(event)=>game.next();
            }
        }
        if(this.step===9)
        {
        alert('Ничья');
        this.board.remove();
        this.board=new Board();
        this.step=0;
        var game=new Game();
        document.getElementById('game').onclick=(event)=>game.next();
        }               
    } 
  
    next():void {
        var el:any=event.target;
        if(el.className=='cell'){
            if(this.step%2===0){
                el.innerHTML=this.X.name;
                el.style.color='blue';
                el.style.pointerEvents='none';         
            }
            else{
                el.innerHTML=this.O.name;
                el.style.color='red';
                el.style.pointerEvents='none';
            }
            this.step++;
            setTimeout(this.checkWinner.bind(this),0);
        }
    }

}
// точка входа
var game:Game=new Game();

